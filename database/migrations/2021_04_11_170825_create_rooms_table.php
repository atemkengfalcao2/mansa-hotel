<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->id();
            $table->double('price')->nullable();
            $table->string('type')->nullable();
            $table->string('title')->nullable();
            $table->integer('number_of_children')->nullable();
            $table->integer('number_of_adults')->nullable();
            $table->integer('air_conditionned')->nullable();
            $table->double('room_size')->nullable();
            $table->integer('state')->default(1);
            $table->text('description')->nullable();
            $table->string('photo1')->nullable();
            $table->string('photo2')->nullable();
            $table->string('photo3')->nullable();
            $table->string('photo4')->nullable();
            $table->timestamp('created_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
