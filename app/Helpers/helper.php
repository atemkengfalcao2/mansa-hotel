<?php

use App\Models\Info;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;

/**
 * Compares given route name with current route name.
 *
 * @param  string $routeName
 * @param string $active
 * @param string $notActive
 * @return bool
 * @internal param string $active
 */
function isActiveRoute($routeName, $active = "active", $notActive = "")
{
    if (Route::currentRouteName() == $routeName) return $active;

    return $notActive;
}

/**
 * Compares given route name with current route prefix.
 * @param $resourceName
 * @param string $active
 * @param string $notActive
 * @return string
 */
function isActiveRouteResourceName($resourceName, $active = "active", $notActive = "")
{

    $name = Route::getCurrentRoute()->getName();

    if (startsWith($name, $resourceName)) return $active;

    return $notActive;
}

/**
 * Compares given URL with current URL.
 *
 * @param  string $url
 * @param  string $active
 * @param string $notActive
 * @return bool
 */
function isActiveURL($url, $active = "active", $notActive = "")
{
    if (URL::current() == url($url)) return $active;

    return $notActive;
}

/**
 * Detects if the given string is found in the current URL.
 *
 * @param  string $string
 * @param  string $active
 * @param string $notActive
 * @return bool
 */
function isActiveMatch($string, $active = "active", $notActive = "")
{
    if (strpos(URL::current(), $string)) return $active;

    return $notActive;
}

/**
 * Compares given array of route names with current route name.
 *
 * @param  array $routeNames
 * @param  string $active
 * @param string $notActive
 * @return bool
 */
function areActiveRoutes(array $routeNames, $active = "active", $notActive = "")
{
    foreach ($routeNames as $routeName) {
        if (Route::currentRouteName() == $routeName) return $active;
    }

    return $notActive;
}

/**
 * Compares given array of route names with current route prefix.
 * @param array $resourcesNames
 * @param string $active
 * @param string $notActive
 * @return string
 */
function areActiveRoutesResourcesNames(array $resourcesNames, $active = "active", $notActive = "")
{
    $name = Route::getCurrentRoute()->getName();

    foreach ($resourcesNames as $resourceName) {
        if (startsWith($name, $resourceName)) return $active;
    }

    return $notActive;
}

/**
 * Compares given array of URLs with current URL.
 *
 * @param  array $urls
 * @param  string $active
 * @param string $notActive
 * @return bool
 */
function areActiveURLs(array $urls, $active = "active", $notActive = "")
{
    foreach ($urls as $url) {
        if (URL::current() == url($url)) return $active;
    }

    return $notActive;
}






function infos()
{
    return Info::first();
}
