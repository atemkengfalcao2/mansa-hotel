<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\RoomRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class RoomCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class RoomCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Room::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/room');
        CRUD::setEntityNameStrings('room', 'rooms');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('id');
        CRUD::column('title')->label('Titre');
        CRUD::column('price')->label('Prix');
        CRUD::column('type');
        CRUD::column('number_of_children')->label("N° d'enfants");
        CRUD::column('number_of_adults')->label("N° d'adultes");
        CRUD::column('air_conditionned')->label("Climatiseur");
        CRUD::column('room_size')->label('Superficie');
        CRUD::column('state')->label('Etat');
        CRUD::column('description')->type('textarea');
        CRUD::column('photo1')->type('image');
        CRUD::column('photo2')->type('image');
        CRUD::column('photo3')->type('image');
        CRUD::column('photo4')->type('image');
        CRUD::column('created_at')->type('datetime')->label('Date de Creation');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(RoomRequest::class);

        CRUD::field('title')->label('Titre');
        CRUD::field('price')->label('Prix');
        CRUD::field('type')->type('select_from_array')->options(['Simple' => 'Simple', 'VIP' =>'VIP', 'Appartement' => 'Appartement'])->default('simple')->allows_null(false)->label('Type de chambre');
        CRUD::field('number_of_children')->type('number')->label("Nomber d'enfants");
        CRUD::field('number_of_adults')->type('number')->label("Nomber d'adultes");;
        CRUD::field('air_conditionned')->type('select_from_array')->options(['climatise' => 'Climatisé', 'non_climatise' => 'Non Climatisé'])->default('climatise')->allows_null(false)->label('Climatisé?');
        CRUD::field('room_size')->label('Superficie');
        CRUD::field('state')->type('select_from_array')->options(['libre' => 'Libre', 'occupe' => 'Occupé'])->default('libre')->allows_null(false)->label('Etat');
        CRUD::field('description')->type('ckeditor');
        CRUD::field('photo1')->type('image')->crop(true)->aspect_ratio(1.6)->label('photo1');
        CRUD::field('photo2')->type('image')->crop(true)->aspect_ratio(1.6)->label('photo2');
        CRUD::field('photo3')->type('image')->crop(true)->aspect_ratio(1.6)->label('photo3');
        CRUD::field('photo4')->type('image')->crop(true)->aspect_ratio(1.6)->label('photo4');
        CRUD::field('photo2');
        CRUD::field('photo3');
        CRUD::field('photo4');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupShowOperation()
    {
        $this->setupListOperation();
    }
}
