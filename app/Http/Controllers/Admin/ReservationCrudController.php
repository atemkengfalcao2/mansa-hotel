<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ReservationRequest;
use App\Models\Customer;
use App\Models\Reservation;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class ReservationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ReservationCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Reservation::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/reservation');
        CRUD::setEntityNameStrings('reservation', 'reservations');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('id');
        CRUD::column('room_title')->label('chambre');
        CRUD::column('customer_name')->label('client');
        CRUD::column('reservation_start')->type('datetime')->label('Arrivé');
        CRUD::column('reservation_end')->type('datetime')->label('Départ');
        CRUD::column('state')->label('Etat');
        CRUD::column('created_at')->type('datetime')->label('Date de Creation');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ReservationRequest::class);

        CRUD::field('name')->label('Nom du client');
        CRUD::field('id_card')->label('Numero de CNI');
        CRUD::field('email')->label('Adresse Email');
        CRUD::field('phone')->label('Telephone');
        CRUD::field('address')->label('Adresse');
        CRUD::field('reservation_start')->type('datetime_picker')->label('Arrivé');
        CRUD::field('reservation_end')->type('datetime_picker')->label('Départ');
        $this->crud->addField([
            'type' => "relationship",
            'name' => 'room_id', // the method on your model that defines the relationship

            // OPTIONALS:
            'label' => "Chambre",
            'attribute' => "title", // foreign key attribute that is shown to user (identifiable attribute)
            'entity' => 'room', // the method that defines the relationship in your Model
            'model' => "App\Models\Room", // foreign key Eloquent model
            'placeholder' => "Selectionnnez une Chambre", // placeholder for the select2 input
            'allows_null' => false,
            // optional - force the related options to be a custom query, instead of all();
            'options'   => (function ($query) {
                return $query->orderBy('title', 'ASC')->where('state', 'libre')->get();
            }), //  you can use this to filter the results show in the select
        ]);

        CRUD::field('state')->type('select_from_array')->options(['actif' => 'Actif', 'inactif' => 'Inactif'])->default('actif')->allows_null(false)->label('Etat');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupShowOperation()
    {
        $this->setupListOperation();
    }

    protected function store(Request $request)
    {
        $customer = new Customer($request->only('name', 'email', 'address', 'id_card', 'phone'));

        $reservation = new Reservation($request->only('reservation_start', 'reservation_end', 'state'));

        DB::transaction(function () use ($request, $customer, $reservation) {
            $customer->save();
            $reservation->customer_id = $customer->id;
            $reservation->room_id = $request->room_id;
            $reservation->save();
        });

        // show a success message
        \Alert::success(trans('backpack::crud.insert_success'))->flash();

        // save the redirect choice for next time
        $this->crud->setSaveAction();

        return $this->crud->performSaveAction($reservation->getKey());
    }

    protected function update($id, Request $request)
    {
        $reservation = Reservation::findOrFail($id);
        $reservation->update($request->only('state', 'reservation_start', 'reservation_end', 'room_id'));

        // dd($reservation->article->tags);
        $reservation->customer->update($request->only('name', 'address','id_card','phone','email'));
        $reservation->customer->save();


        // show a success message
        \Alert::success(trans('backpack::crud.update_success'))->flash();

        // save the redirect choice for next time
        $this->crud->setSaveAction();

        return $this->crud->performSaveAction($reservation->getKey());
    }

}
