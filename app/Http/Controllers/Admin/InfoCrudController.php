<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\InfoRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class InfoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class InfoCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Info::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/info');
        CRUD::setEntityNameStrings('info', 'infos');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('logo')->type('image')->label('Logo');
        CRUD::column('name')->label("Nom de l'entreprise");
        CRUD::column('title')->label('Slogan');
        CRUD::column('email')->type('email')->label('Email');
        CRUD::column('phone')->label('Tel');
        CRUD::column('address')->label('Adresse');
        CRUD::column('founded')->label('Date de Creation');
        CRUD::column('whatsapp');
        CRUD::column('telegram');
        CRUD::column('instagram');
        CRUD::column('facebook');
        CRUD::column('youtube');
        CRUD::column('twitter');
        CRUD::column('description')->type('textarea')->label('Description');
        $this->crud->addColumn([
            'name'  => 'about',
            'label' => 'A propos',
            'type'  => 'table',
            'columns' => [
                'description'  => 'Description',
                'cover'        => 'Cover',
            ]
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(InfoRequest::class);

        CRUD::field('state')->type('hidden')->value(1);
        CRUD::field('name')->label("Nom de l'entreprise");
        CRUD::field('title')->label('Slogan');
        CRUD::field('email')->type('email')->label('Email');
        CRUD::field('phone')->label('Tel');
        CRUD::field('address')->label('adresse');
        CRUD::field('logo')->type('image')->crop(true)->aspect_ratio(1.8)->label('Logo');
        CRUD::field('theme');
        CRUD::field('whatsapp');
        CRUD::field('telegram');
        CRUD::field('instagram');
        CRUD::field('facebook');
        CRUD::field('youtube');
        CRUD::field('twitter');
        CRUD::field('description')->type('ckeditor')->label('Description');
        $this->crud->addField([   // repeatable
            'name'  => 'about',
            'label' => 'A Propos',
            'type'  => 'repeatable',
            'fields' => [
                [
                    'name'    => 'cover',
                    'type'    => 'image',
                    'label'   => 'Cover',
                    'aspect_ratio' => 1,
                    'crop' => true,
                ],
                [
                    'name'  => 'description',
                    'type'  => 'ckeditor',
                    'label' => 'Description',
                ],
            ],

            // optional
            'init_rows' => 1, // number of empty rows to be initialized, by default 1
            'min_rows' => 1, // minimum rows allowed, when reached the "delete" buttons will be hidden
            'max_rows' => 1 // maximum rows allowed, when reached the "new item" button will be hidden

        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupShowOperation()
    {
        $this->setupListOperation();
    }
}
