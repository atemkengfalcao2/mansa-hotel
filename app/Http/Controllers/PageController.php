<?php

namespace App\Http\Controllers;

use App\Models\Room;
use Illuminate\Http\Request;

class PageController extends Controller
{

    public function index()
    {
        $data = [];

        return view('home', compact('data'));
    }

    public function about()
    {
        $data = [];

        return view('about', compact('data'));
    }

    public function contact()
    {
        $data = [];

        return view('contact', compact('data'));
    }

    public function rooms()
    {
        $data = [];

        return view('rooms', compact('data'));
    }

    public function room(Room $room)
    {
        $data = [];

        return view('room', compact('room','data'));
    }

    public function gallery()
    {
        $data = [];

        return view('gallery', compact('data'));
    }
}
