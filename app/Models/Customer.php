<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory;

    protected $guarded = ['id'];
    public $timestamps = null;

    public function rooms()
    {
        return $this->belongsToMany('App\Models\Room', 'reservations');
    }

    public function reservations()
    {
        return $this->hasMany('App\Models\Reservation');
    }


    /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */
    public function getStateAttribute()
    {
        if ($this->attributes['state'] == 0) return 'inactif';
        return 'actif';
    }

    /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
    public function setStateAttribute($value)
    {
        if ($value == 'actif') $this->attributes['state'] = 1;
        else $this->attributes['state'] = 0;
    }

}
