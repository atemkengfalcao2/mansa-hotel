<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory;

    protected $guarded = ['id'];
    public $timestamps = null;

    public function room(){
        return $this->belongsTo('App\Models\Room');
    }

    public function customer(){
        return $this->belongsTo('App\Models\Customer');
    }

    /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */
    public function getStateAttribute()
    {
        if ($this->attributes['state'] == 0) return 'inactif';
        return 'actif';
    }

    public function getCustomerNameAttribute(){
        return $this->customer->name;
    }

    public function getRoomTitleAttribute(){
        return $this->room->title;
    }

    public function getNameAttribute(){
        return $this->customer->name;
    }

    public function getEmailAttribute()
    {
        return $this->customer->email;
    }

    public function getPhoneAttribute()
    {
        return $this->customer->phone;
    }

    public function getAddressAttribute()
    {
        return $this->customer->address;
    }

    public function getIdCardAttribute()
    {
        return $this->customer->id_card;
    }

    /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
    public function setStateAttribute($value)
    {
        if ($value == 'actif') $this->attributes['state'] = 1;
        else $this->attributes['state'] = 0;
    }
}
