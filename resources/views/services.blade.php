@extends('Layouts.base')

@section('content')

    <!-- Banner  -->
		<div class="dlab-bnr-inr overlay-primary-dark" style="background-image: url(images/banner/bnr1.jpg);">
			<div class="container">
				<div class="dlab-bnr-inr-entry">
					<h1>Services 1</h1>
					<!-- Breadcrumb Row -->
					<nav aria-label="breadcrumb" class="breadcrumb-row">
						<ul class="breadcrumb">
							<li class="breadcrumb-item"><a href="index.html">Home</a></li>
							<li class="breadcrumb-item active" aria-current="page">Services</li>
						</ul>
					</nav>
					<!-- Breadcrumb Row End -->
				</div>
			</div>
		</div>
		<!-- Banner End -->

		<!-- Service -->
		<section class="content-inner" style="background-image: url(../template/images/background/bg1.png);">
			<div class="container">
				<div class="row">
					@foreach (App\Models\Service::all()->where('state','actif') as $serv)
                    <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">
                        <div class="icon-bx-wraper style-1 box-hover text-center m-b30">
                            <div class="icon-bx-md radius bg-{{$serv->color}} shadow-{{$serv->color}}">
                                <a href="javascript:void(0);" class="icon-cell">
                                    <i class="flaticon-office"></i>
                                </a>
                            </div>
                            <div class="icon-content">
                                <h4 class="dlab-title">{{$serv->name}}</h4>
                                {!! $serv->description !!}
                            </div>
                        </div>
                    </div>
                    @endforeach
				</div>
			</div>
		</section>

		<!-- Newsletter -->
		<section class="content-inner-3 bg-primary" style="background-image: url(images/background/bg13.png); background-repeat: no-repeat; background-size: cover;">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-4 col-md-6 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.2s">
						<div class="dlab-media m-b30">
							<img src="template/images/about/img9.png" class="move-2" alt="">
						</div>
					</div>
					<div class="col-lg-8 col-md-6 wow fadeInRight" data-wow-duration="2s" data-wow-delay="0.4s">
						<div class="section-head style-1 text-white">
							<h6 class="sub-title bgl-light m-b20">Newsletter</h6>
							<h2 class="title m-b30">Subscribe To Our Newsletter For Latest Update Of Finanical Services </h2>
							<p>Curabitur eleifend nibh sit amet ex posuere, vel malesuada turpis pretium. Quisque et tincidunt risus, a tempor massa. Cras tempor egestas libero, eu laoreet enim pharetra non.</p>
						</div>
						<div class="dlab-subscribe style-2 max-w500">
							<form class="dzSubscribe" action="script/mailchamp.php" method="post">
								<div class="dzSubscribeMsg"></div>
								<div class="form-group">
									<div class="input-group">
										<input name="dzEmail" required="required" type="email" class="form-control" placeholder="Email Address">
										<div class="input-group-addon">
											<button name="submit" value="Submit" type="submit" class="btn rounded-xl shadow btn-primary">Subscribe Now</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- Fetures -->
		<section class="content-inner-2">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-xl-6 col-lg-7 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.2s">
						<div class="section-head style-1">
							<h6 class="sub-title bgl-primary m-b20 text-primary">Fetures</h6>
							<h2 class="title">Our Working Process To Help Your Boost Your Business</h2>
						</div>
						<div class="section-wraper-one">
							<div class="icon-bx-wraper style-2 left m-b30">
								<div class="icon-bx-md radius bg-white text-red">
									<a href="javascript:void(0);" class="icon-cell">
										<i class="flaticon-idea"></i>
									</a>
								</div>
								<div class="icon-content">
									<h4 class="dlab-title">Idea & Analysis Gathering</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								</div>
							</div>
							<div class="icon-bx-wraper style-2 left m-b30">
								<div class="icon-bx-md radius bg-white text-yellow">
									<a href="javascript:void(0);" class="icon-cell">
										<i class="flaticon-line-graph"></i>
									</a>
								</div>
								<div class="icon-content">
									<h4 class="dlab-title">Designing & Developing</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et.</p>
								</div>
							</div>
							<div class="icon-bx-wraper style-2 left m-b30">
								<div class="icon-bx-md radius bg-white text-green">
									<a href="javascript:void(0);" class="icon-cell">
										<i class="flaticon-rocket"></i>
									</a>
								</div>
								<div class="icon-content">
									<h4 class="dlab-title">Testing & Lunching</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-6 col-lg-5 wow fadeInRight" data-wow-duration="2s" data-wow-delay="0.4s">
						<div class="dlab-media">
							<img src="template/images/about/img5.png" class="move-2" alt="">
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- Testimonials -->
		<section class="content-inner bg-primary" style="background-image: url(images/background/bg3.png);">
			<div class="px-xl-5 px-md-4 px-3">
				<div class="row testimonials-wraper-1">
					<div class="col-lg-9">
						<div class="testimonials-carousel1 owl-carousel owl-theme owl-btn-2 owl-btn-white owl-btn-center">
							@foreach (App\Models\Testimony::all()->where('state','actif') as $tes)
                            <div class="item wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">
                                <div class="testimonial-1">
                                    <div class="testimonial-text">
                                        {!! $tes->testimony !!}
                                    </div>
                                    <div class="testimonial-detail">
                                        <div class="testimonial-pic">
                                            <img src="{{asset($tes->photo)}}" alt="">
                                        </div>
                                        <div class="clearfix">
                                            <strong class="testimonial-name">{{$tes->name}}</strong>
                                            <span class="testimonial-position">{{$tes->job}} </span>
                                            <ul class="star-rating">
                                                @for ($i = 0; $i < $tes->rating; $i++)
                                                    <li><i class="fa fa-star text-warning"></i></li>
                                                    @endfor
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
						</div>
					</div>
					<div class="col-lg-3 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.6s">
						<div class="section-head style-1 text-white p-t15">
							<h6 class="sub-title bgl-light m-b20">Testmonial</h6>
							<h2 class="title">See What Are They Say About Us </h2>
							<p>Fusce vitae sapien eu mauris semper faucibus eget tristique lorem.</p>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- Call To action -->
		<section style="background-image: url(images/background/bg5.jpg); background-size: cover;">
			<div class="container">
				<div class="row action-box style-1 align-items-center">
					<div class="col-xl-7 col-lg-8 col-md-8 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">
						<div class="section-head style-1">
							<h6 class="sub-title bgl-primary m-b20 text-primary">More With Us</h6>
							<h2 class="title">You Want To Showcase Your Website In Top Join With Us</h2>
						</div>
					</div>
					<div class="col-xl-5 col-lg-4 col-md-4 text-right m-b30 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.4s">
						<a href="contact-us-1.html" class="btn btn-link d-inline-flex align-items-center"><i class="fa fa-angle-right m-r10"></i>Join Now</a>
					</div>
				</div>
			</div>
		</section>

@endsection
