@extends('Layouts.base')

@section('content')
    <!-- ================ Inner banner ================ -->
<div class="inner-banner inner-banner-bg pt-70 pb-40">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-8 col-md-8 mb-30">
        <!-- page-title -->
        <div class="page-title">
          <h1>{{$room->title}}</h1>
        </div>
        <!-- page-title end -->
      </div>
      <div class="col-lg-4 col-md-4 mb-30">
        <!-- breadcrumb -->
        <ol class="breadcrumb mb-0">
          <li class="breadcrumb-item"><a href="#">Accueil</a></li>
          <li class="breadcrumb-item active">Chambre</li>
        </ol>
        <!-- breadcrumb end -->
      </div>
    </div>
  </div>
</div>
<!-- ================ Inner banner end ================ -->

<!-- ================ Detail page ================ -->
<div class="detail-page pt-70 pb-40">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-8">
        <div class="title">
          <h2>{{$room->title}}</h2>
          <p>{{$room->type}} - {{$room->room_size}}m2</p>
        </div>
        <!-- detail page gallery -->
        <div class="owl-carousel detail-page-gallery-carousel mb-20">
            @for ($i = 1; $i < 4; $i++)
                @if ($room->{'photo'.$i})
                    <figure class="item mb-0"> <img src="{{asset($room->{'photo'.$i})}}" alt="img description"> </figure>
                @endif
            @endfor
        </div>
        <!-- detail page gallery end -->
        <!-- tabs -->
        <div class="detail-tabs mb-30">
          <ul class="nav nav-tabs nav-pills nav-justified" id="myTab" role="tablist">
            <li class="nav-item"> <a class="nav-link active" id="tab1-tab" data-toggle="tab" href="#tab1" role="tab" aria-controls="tab1" aria-selected="true">Apperçu</a> </li>
            <li class="nav-item"> <a class="nav-link" id="tab2-tab" data-toggle="tab" href="#tab2" role="tab" aria-controls="tab2" aria-selected="false">Details</a> </li>
          </ul>
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active p-15" id="tab1" role="tabpanel" aria-labelledby="tab1-tab">
              <!-- overview -->
              <h4 class="mb-6">Description</h4>
              {!! $room->description !!}
              <!-- overview end -->
            </div>
            <div class="tab-pane fade p-15" id="tab2" role="tabpanel" aria-labelledby="tab2-tab">
              <!-- ameneties -->
              <h4 class="mb-6"> Details </h4>
              <div class="row">
                <div class="col-lg-4">
                  <ul class="ameneties-list">
                    <li><i class="fas fa-user pr-6"></i> {{$room->number_of_adults}} Adulte(s)</li>
                    <li><i class="fas fa-people-carry pr-6"></i> {{$room->number_of_children}} Enfant(s)</li>
                    <li><i class="fas fa-shower pr-6"></i> {{$room->air_conditionned}} </li>
                  </ul>
                </div>
              </div>
              <!-- ameneties end -->
            </div>
          </div>
        </div>
        <!-- tabs end -->
      </div>
      <div class="col-lg-4 col-md-4">
        <aside>
          <!-- help us -->
          <div class="help-us mb-30">
            <h3>Comment pouvons nous vous aidez ?</h3>
            <p>En laissant une spéecificité nous pouvons mieux vous satisfaire.</p>
            <a class="view-detail-btn" href="#"><i class="fas fa-phone-alt"></i> Contactez Nous</a> </div>
          <!-- help us end -->
        </aside>
      </div>
    </div>
  </div>
</div>
<!-- ================ Detail page end ================ -->
@endsection
