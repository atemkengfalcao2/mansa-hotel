@extends('Layouts.base')

@section('content')
<!-- ================ Slider area ================ -->
<div class="slider">
    <!-- search area -->
    <div class="search-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-8 col-sm-12 col-12 offset-xl-0 offset-lg-3 offset-sm-0">
                    <div class="center-search">
                        <h1 class="text-white">profitez de votre sejour</h1>
                        <p class="text-white">Recherche et Livre Hotel</p>
                        <form class="form-style-1">

                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="check-in" placeholder="Check In">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="check-out" placeholder="Check Out">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option>Adulte(s)(18+)</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option>Enfants(0-17)</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <select class="form-control">
                                    <option>Chambres</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                </select>
                            </div>
                            <button type="submit" class="btn-style-1 w-100">Recherche</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- search area end -->
    <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner">

            <!-- slider item -->
            <div class="carousel-item active"> <img src="template/img/slider/slider-2.jpg" alt="" class="img-fluid">
            </div>
            <!-- slider item end -->
            <!-- slider item -->
            <div class="carousel-item"> <img src="template/img/slider/slider-3.jpg" alt="" class="img-fluid"> </div>
            <!-- slider item end -->
            <!-- slider item -->
            <div class="carousel-item"> <img src="template/img/slider/aerien.jpg" alt="" class="img-fluid"> </div>
            <!-- slider item end -->
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev"> <i
                class="fas fa-caret-left"></i></a> <a class="carousel-control-next" href="#myCarousel" role="button"
            data-slide="next"> <i class="fas fa-caret-right"></i></a>
    </div>
</div>
<!-- ================ Slider area end ================ -->

<!-- ================ About area ================ -->
<div class="about-area pt-70 pb-60">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 mb-30">
                <!-- about text -->
                <div class="about-col">
                    <h6>A Propos de Nous</h6>
                    <h2><span>Chez nous, le client est Roi </span></h2>
                    <p>L'hôtel MANSA conjugue à la fois une tradition de l’hébergement, une restauration classique et
                        exotique.</p><br>
                    <p>L'hotel Mansa un endroit plus que paradisiaque pour un sejour entre amis ou en famille</p>

                    <a class=" btn-style-1" href="#" role="button">Explore More <i
                            class="fas fa-long-arrow-alt-right pl-6"></i></a>
                </div>
                <!-- about text end -->
            </div>
            <div class="col-lg-6 mb-30">
                <!-- about video -->
                <div class="about-img-box position-relative rounded"> <img src="template/img/about-us/espace.jpg" alt=""
                        class="img-fluid rounded">
                    <div class="hoverlay d-flex flex-row align-items-center justify-content-center"> <a
                            class="venobox d-inline" data-gall="gall-video" data-autoplay="true" data-vbtype="video"
                            href="https://www.youtube.com/watch?v=668nUCeBHyY&amp;t=28s"><i
                                class="fab fa-google-play"></i></a> </div>
                </div>
                <!-- about video end -->
            </div>
        </div>
    </div>
</div>
<!-- ================ About area end ================ -->

<!-- ================ Most popular hotel ================ -->
<div class="most-popular-hotel pt-70 pb-70 position-relative">
    <div class="bg-style-1"></div>
    <div class="container">
        <!-- section title -->
        <div class="section-title text-center">
            <h2><strong>Exposition de nos Chambres & Suites</strong></h2>
            <span class="dashed-border"></span>
        </div>
        <!-- section title -->
    </div>
    <div class="container-fluid">
        <!-- popular hotel carousel -->
        <div class="popular-hotel-carousel owl-carousel owl-theme">
            @foreach (App\Models\Room::all()->take(8) as $room)
                <div class="item">
                    <!-- popular hotel box -->
                    <div class="popular-hotel-box">
                        <div class="imege mb-10"><img src="{{asset($room->photo1)}}" alt="">
                            <div class="black-bg"> </div>
                        </div>
                        <h4><a href={{route('room', $room->id)}}>{{$room->type}}</a></h4>
                        <div class="price">{{$room->price}} Fcfa / <span>Par nuit</span></div>
                    </div>
                    <!-- popular hotel box end -->
                </div>
            @endforeach
        </div>
        <!-- popular hotel carousel end -->
    </div>
</div>
<!-- ================ Most popular hotel end ================ -->

<!-- ================ galleris photo hotel================ -->
<div class="popular-destinations pt-70 pb-40 position-relative">
    <div class="bg-style-1"></div>
    <div class="container">
        <!-- section title -->
        <div class="section-title text-center">
            <h2>Galleries</h2>
            <span class="dashed-border"></span>
        </div>
        <!-- section title -->
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6 mb-30">
                <!-- popular destination box -->
                <div class="popular-destination-box">
                    <div class="img-holder-overlay">
                        <div class="img-holder"><img src="template/img/popular-destination/img-04.jpg" alt=""></div>
                        <div class="overlay"><a href="#"><i class="fas fa-share"></i></a></div>
                    </div>
                    <div class="title">
                        <h3><a href="#">Chambres</a></h3>
                    </div>
                </div>
                <!-- popular destination box end -->
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 mb-30">
                <!-- popular destination box -->
                <div class="popular-destination-box">
                    <div class="img-holder-overlay">
                        <div class="img-holder"><img src="template/img/popular-destination/img-07.jpg" alt=""></div>
                        <div class="overlay"><a href="#"><i class="fas fa-share"></i></a></div>
                    </div>
                    <div class="title">
                        <h3><a href="#">Piscine</a></h3>
                    </div>
                </div>
                <!-- popular destination box end -->
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 mb-30">
                <!-- popular destination box -->
                <div class="popular-destination-box">
                    <div class="img-holder-overlay">
                        <div class="img-holder"><img src="template/img/popular-destination/img-08.jpg" alt=""></div>
                        <div class="overlay"><a href="#"><i class="fas fa-share"></i></a></div>
                    </div>
                    <div class="title">
                        <h3><a href="#">Salle conference</a></h3>
                    </div>
                </div>
                <!-- popular destination box end -->
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 mb-30">
                <!-- popular destination box -->
                <div class="popular-destination-box">
                    <div class="img-holder-overlay">
                        <div class="img-holder"><img src="template/img/popular-destination/4.jpg" alt=""></div>
                        <div class="overlay"><a href="#"><i class="fas fa-share"></i></a></div>
                    </div>
                    <div class="title">
                        <h3><a href="#">Vue Sur la Ville</a></h3>
                    </div>
                </div>
                <!-- popular destination box end -->
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 mb-30">
                <!-- popular destination box -->
                <div class="popular-destination-box">
                    <div class="img-holder-overlay">
                        <div class="img-holder"><img src="template/img/popular-destination/5.jpg" alt=""></div>
                        <div class="overlay"><a href="#"><i class="fas fa-share"></i></a></div>
                    </div>
                    <div class="title">
                        <h3><a href="#">Musée</a></h3>
                    </div>
                </div>
                <!-- popular destination box end -->
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 mb-30">
                <!-- popular destination box -->
                <div class="popular-destination-box">
                    <div class="img-holder-overlay">
                        <div class="img-holder"><img src="template/img/popular-destination/6.jpg" alt=""></div>
                        <div class="overlay"><a href="#"><i class="fas fa-share"></i></a></div>
                    </div>
                    <div class="title">
                        <h3><a href="#">Oeuvre D'Art</a></h3>
                    </div>
                </div>
                <!-- popular destination box end -->
            </div>
        </div>
    </div>
</div>
<!-- ================ Popular destinations end ================ -->

<!-- ================ Testimonials ================ -->
<div class="testimonials-area testimonial-bg  pt-70 pb-70">
    <div class="container">
        <div class="testimonial-carousel owl-carousel owl-theme">
            @foreach (App\Models\Testimony::all() as $testimony)
            <div class="item">
                <!-- testimonial box -->
                <div class="testimonial-box position-relative shadow rounded">
                    <p class="text-muted border-bottom font-italic pb-3">
                        {!! $testimony->testimony !!}
                    </p>
                    <div class="reviewer d-inline-block"> <img src="template/img/testimonial/Samuel.jpg"
                            class="float-left mr-3 rounded" alt="">
                        <div class="content d-block overflow-hidden">
                            <h4 class="name mb-0 text-uppercase">{{$testimony->name}}</h4>
                            <small class="designation text-muted">{{$testimony->occupation}}</small>
                        </div>
                    </div>
                    <div class="quote-icon"><i class="fas fa-quote-left"></i></div>
                </div>
                <!-- testimonial box end -->
            </div>
            @endforeach
        </div>
    </div>
</div>
<!-- ================ Testimonials end ================ -->


@endsection
