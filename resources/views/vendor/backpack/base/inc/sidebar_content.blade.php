<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('customer') }}'><i class='nav-icon la la-question'></i> Customers</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('room') }}'><i class='nav-icon la la-question'></i> Rooms</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('reservation') }}'><i class='nav-icon la la-question'></i> Reservations</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('testimony') }}'><i class='nav-icon la la-question'></i> Testimonies</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('newletter') }}'><i class='nav-icon la la-question'></i> Newletters</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('info') }}'><i class='nav-icon la la-question'></i> Infos</a></li>
