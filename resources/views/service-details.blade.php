@extends('Layouts.base')

@section('content')
<!-- Banner  -->
<div class="dlab-bnr-inr overlay-primary-dark" style="background-image: url(template/images/banner/bnr1.jpg);">
    <div class="container">
        <div class="dlab-bnr-inr-entry">
            <h1>Services Details 1</h1>
            <!-- Breadcrumb Row -->
            <nav aria-label="breadcrumb" class="breadcrumb-row">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Services Details</li>
                </ul>
            </nav>
            <!-- Breadcrumb Row End -->
        </div>
    </div>
</div>
<!-- Banner End -->

<!-- Services Details -->
<section class="content-inner-2">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4 m-b30">
                <aside class="side-bar sticky-top">
                    <div class="widget_nav_menu m-b40">
                        <ul>
                            <li><a href="services-details-1.html">ALL SERVICES</a> </li>
                            <li><a href="services-details-1.html">Strategy & Research</a></li>
                            <li class="active"><a href="services-details-1.html">Web Development</a> </li>
                            <li><a href="services-details-1.html">Web Solution</a> </li>
                            <li><a href="services-details-1.html">Company Branding</a> </li>
                            <li><a href="services-details-1.html">SEO & Marketing</a> </li>
                            <li><a href="services-details-1.html">24X7 Support</a> </li>
                        </ul>
                    </div>
                    <div class="widget widget_bunch_brochures">
                        <div class="download-file">
                            <h4 class="title">Get your brochures</h4>
                            <ul>
                                <li>
                                    <a href="#" target="_blank">
                                        <div class="text">Company Brochures</div>
                                        <i class="fa fa-download"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" target="_blank">
                                        <div class="text">Company Info</div>
                                        <i class="fa fa-download"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </aside>
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="dlab-media m-b30 rounded-md">
                    <img src="images/blog/default/thum1.jpg" alt="">
                </div>
                <div class="dlab-content">
                    <div class="m-b40">
                        <h3>Web Development</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                            It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum
                            passages, and more recently with desktop publishing software like Aldus PageMaker including
                            versions of Lorem Ipsum.</p>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <h3>How You Start Web Development ?</h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley.</p>
                            <ul class="list-check style-1 primary">
                                <li>Lorem Ipsum is simply dummy text of the printing.</li>
                                <li>Lorem Ipsum has been the industry's standard dummy text ever.</li>
                                <li>when an unknown printer took a galley of type and scrambled.</li>
                                <li>It has survived not only five centuries, but also the leap.</li>
                                <li>Recently with desktop publishing software like Aldus PageMaker.</li>
                                <li>Lorem Ipsum is simply dummy text of the printing industry.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
