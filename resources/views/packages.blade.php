@extends('Layouts.base')

@section('content')
    <!-- Banner  -->
    <div class="dlab-bnr-inr overlay-primary-dark" style="background-image: url(images/banner/bnr1.jpg);">
        <div class="container">
            <div class="dlab-bnr-inr-entry">
                <h1>Pricing Table</h1>
                <!-- Breadcrumb Row -->
                <nav aria-label="breadcrumb" class="breadcrumb-row">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Pricing Table</li>
                    </ul>
                </nav>
                <!-- Breadcrumb Row End -->
            </div>
        </div>
    </div>
    <!-- Banner End -->

    <!-- Pricing Table -->
    <section class="content-inner" style="background-image: url(images/background/bg1.png);">
        <div class="container">
            <div class="pricingtable-row">
                <div class="row">
                    @foreach (App\Models\Pack::all()->where('state','actif') as $pack)
                        <div class="col-lg-4 col-md-6 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">
                            <div class="pricingtable-wrapper style-1 m-b30">
                                <div class="pricingtable-inner">
                                    <div class="pricingtable-title">
                                        <h3>{{$pack->name}}</h3>
                                    </div>
                                    <div class="pricingtable-price">
                                        <h2 class="pricingtable-bx"> {{$pack->price}} </h2>
                                    </div>
                                    <ul class="pricingtable-features">
                                        @foreach ($pack->services as $serv)
                                            <li> {{$serv->name}} </li>
                                        @endforeach
                                    </ul>
                                    <div class="pricingtable-footer">
                                        <a href="javascript:void(0);" class="btn btn-link d-inline-flex align-items-center"><i
                                                class="fa fa-angle-right m-r10"></i>Start Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection
