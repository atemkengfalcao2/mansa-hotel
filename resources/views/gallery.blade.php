@extends('Layouts.base')

@section('content')
    <!-- ================ Inner banner ================ -->
    <div class="inner-banner inner-banner-bg pt-70 pb-40">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-8 col-md-8 mb-30">
                    <!-- page-title -->
                    <div class="page-title">
                        <h1>Gallerie</h1>
                    </div>
                    <!-- page-title end -->
                </div>
                <div class="col-lg-4 col-md-4 mb-30">
                    <!-- breadcrumb -->
                    <ol class="breadcrumb mb-0">
                        <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                        <li class="breadcrumb-item active">Gallerie Photo</li>
                    </ol>
                    <!-- breadcrumb end -->
                </div>
            </div>
        </div>
    </div>
    <!-- ================ Inner banner end ================ -->

    <!-- ================ Gallery page ================ -->
    <div class="gallery-page pt-70 pb-40">
        <div class="container">
            <div class="row row-cols-lg-3 row-cols-md-3 row-cols-sm-2 row-cols-1">
                <div class="col mb-30">
                    <!-- gallery box -->
                    <div class="gallery-box">
                        <div class="img-holder-overlay">
                            <div class="img-holder"><img src="template/img/gallery/1.jpg" alt=""></div>
                            <div class="overlay"><a href="template/img/gallery/1.jpg" class="venobox" data-gall="gallery1"><i
                                        class="far fa-image"></i></a></div>
                        </div>
                        <div class="title">
                            <h3>Chambre simple</h3>
                        </div>
                    </div>
                    <!-- gallery box end -->
                </div>
                <div class="col mb-30">
                    <!-- gallery box -->
                    <div class="gallery-box">
                        <div class="img-holder-overlay">
                            <div class="img-holder"><img src="template/img/gallery/2.jpg" alt=""></div>
                            <div class="overlay"><a href="template/img/gallery/2.jpg" class="venobox" data-gall="gallery1"><i
                                        class="far fa-image"></i></a></div>
                        </div>
                        <div class="title">
                            <h3>Piscine</h3>
                        </div>
                    </div>
                    <!-- gallery box end -->
                </div>
                <div class="col mb-30">
                    <!-- gallery box -->
                    <div class="gallery-box">
                        <div class="img-holder-overlay">
                            <div class="img-holder"><img src="template/img/gallery/3.jpg" alt=""></div>
                            <div class="overlay"><a href="template/img/gallery/3.jpg" class="venobox" data-gall="gallery1"><i
                                        class="far fa-image"></i></a></div>
                        </div>
                        <div class="title">
                            <h3>Suites</h3>
                        </div>
                    </div>
                    <!-- gallery box end -->
                </div>
                <div class="col mb-30">
                    <!-- gallery box -->
                    <div class="gallery-box">
                        <div class="img-holder-overlay">
                            <div class="img-holder"><img src="template/img/gallery/4.jpg" alt=""></div>
                            <div class="overlay"><a href="template/img/gallery/4.jpg" class="venobox" data-gall="gallery1"><i
                                        class="far fa-image"></i></a></div>
                        </div>
                        <div class="title">
                            <h3>chambre Double</h3>
                        </div>
                    </div>
                    <!-- gallery box end -->
                </div>
                <div class="col mb-30">
                    <!-- gallery box -->
                    <div class="gallery-box">
                        <div class="img-holder-overlay">
                            <div class="img-holder"><img src="template/img/gallery/tennis.webp" alt=""></div>
                            <div class="overlay"><a href="template/img/gallery/tennis.webp" class="venobox" data-gall="gallery1"><i
                                        class="far fa-image"></i></a></div>
                        </div>
                        <div class="title">
                            <h3>Tennis</h3>
                        </div>
                    </div>
                    <!-- gallery box end -->
                </div>
                <div class="col mb-30">
                    <!-- gallery box -->
                    <div class="gallery-box">
                        <div class="img-holder-overlay">
                            <div class="img-holder"><img src="template/img/gallery/6.jpg" alt=""></div>
                            <div class="overlay"><a href="template/img/gallery/6.jpg" class="venobox" data-gall="gallery1"><i
                                        class="far fa-image"></i></a></div>
                        </div>
                        <div class="title">
                            <h3>Appartement</h3>
                        </div>
                    </div>
                    <!-- gallery box end -->
                </div>
                <div class="col mb-30">
                    <!-- gallery box -->
                    <div class="gallery-box">
                        <div class="img-holder-overlay">
                            <div class="img-holder"><img src="template/img/gallery/aerien.jpg" alt=""></div>
                            <div class="overlay"><a href="template/img/gallery/aerien.jpg" class="venobox" data-gall="gallery1"><i
                                        class="far fa-image"></i></a></div>
                        </div>
                        <div class="title">
                            <h3>vue aerien</h3>
                        </div>
                    </div>
                    <!-- gallery box end -->
                </div>
                <div class="col mb-30">
                    <!-- gallery box -->
                    <div class="gallery-box">
                        <div class="img-holder-overlay">
                            <div class="img-holder"><img src="template/img/gallery/suite7.jpg" alt=""></div>
                            <div class="overlay"><a href="template/img/gallery/suite7.webp" class="venobox" data-gall="gallery1"><i
                                        class="far fa-image"></i></a></div>
                        </div>
                        <div class="title">
                            <h3>Restaurant</h3>
                        </div>
                    </div>
                    <!-- gallery box end -->
                </div>
                <div class="col mb-30">
                    <!-- gallery box -->
                    <div class="gallery-box">
                        <div class="img-holder-overlay">
                            <div class="img-holder"><img src="template/img/gallery/suiteluxe.jpg" alt=""></div>
                            <div class="overlay"><a href="template/img/gallery/suiteluxe.jpg" class="venobox" data-gall="gallery1"><i
                                        class="far fa-image"></i></a></div>
                        </div>
                        <div class="title">
                            <h3>Suite luxieuse</h3>
                        </div>
                    </div>
                    <!-- gallery box end -->
                </div>
            </div>
        </div>
    </div>
    <!-- ================ Gallery page end ================ -->
@endsection
