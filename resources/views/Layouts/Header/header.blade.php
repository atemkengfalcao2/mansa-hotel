<header class="header">
    <!-- header upper -->
    @include('Layouts.Header.navbar')
    <!-- header upper end -->
    <!-- header lover -->
    <div class="header-lover">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-3 col-md-12">
                    <!-- brand -->
                    <div class="logo"><a class="navbar-brand p-0" href={{route('home')}}><img src="{{asset(infos()->logo)}}" alt=""></a>
                    </div>
                    <!-- brand end -->
                </div>
                <div class="col-lg-9 col-md-12">
                    <!-- header call us -->
                    <div class="header-call-us">
                        <ul>
                            <li>
                                <div class="iocn-holder"><i class="far fa-clock"></i></div>
                                <div class="text-holder">
                                    <h6>Heurs de Travail</h6>
                                    <p class="mb-0">Lundi - Dimanche: 24h/24</p>
                                </div>
                            </li>
                            <li>
                                <div class="iocn-holder"><i class="fas fa-phone-volume"></i></div>
                                <div class="text-holder">
                                    <h6>Appelez nous</h6>
                                    <p class="mb-0"><a href="tel:{{infos()->phone}}">+237 {{infos()->phone}}</a></p>
                                </div>
                            </li>
                            <li>
                                <div class="iocn-holder"><i class="far fa-envelope"></i></div>
                                <div class="text-holder">
                                    <h6>Ecrivez nous</h6>
                                    <p class="mb-0"><a href="mailto:{{infos()->email}}">{{infos()->email}}</a></p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!-- header call us end -->
                </div>
            </div>
        </div>
    </div>
    <!-- header lover end -->
</header>
