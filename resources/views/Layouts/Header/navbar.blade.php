<div class="header-upper-bar">
    <div class="container">
        <div class="row d-flex align-items-center">
            <div class="col-lg-8 col-md-6 col-sm-4 col-2">
                <!-- header navigation -->
                <nav class="navbar header-navigation navbar-expand-lg p-0">
                    <!-- mobile Toggle -->
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTheme"
                        aria-controls="navbarTheme" aria-expanded="false" aria-label="Toggle navigation">
                        <span></span> <span></span> <span></span> </button>
                    <!-- mobile toggle end -->
                    <!-- top Menu -->
                    <div class="collapse navbar-collapse" id="navbarTheme">
                        <ul class="navbar-nav align-items-start align-items-lg-center">
                            <li class="{{isActiveRoute('home')}}"><a class="nav-link" href={{route('home')}}>Accueil</a></li>
                            <li class="{{isActiveRoute('gallery')}}"><a class="nav-link" href={{route('gallery')}}>Galerie</a></li>
                            <li class="{{isActiveRoute('rooms')}}"><a class="nav-link" href={{route('rooms')}}>Chambres</a></li>
                            <li class="{{isActiveRoute('about')}}"><a class="nav-link" href={{route('about')}}>A propos</a></li>
                            <li class="{{isActiveRoute('contact')}}"><a class="nav-link" href={{route('contact')}}>Contactez nous</a></li>
                        </ul>
                    </div>
                    <!-- top menu end -->
                </nav>
                <!-- header navigation end -->
            </div>
            <div class="col-lg-4 col-md-6 col-sm-8 col-10 text-right">
                <!-- header right link -->
                <div class="header-right-link">
                    <ul>
                        {{-- <li><a href="signin.html"><i class="fas fa-chevron-right"></i> Connexion</a></li>
                        <li><a href="register.html"><i class="fas fa-chevron-right"></i> Inscription</a></li>
                        <li><a href="contact-us.html" class="header-request">demande un Avis</a></li> --}}
                    </ul>
                </div>
                <!-- header right link end -->
            </div>
        </div>
    </div>
</div>
