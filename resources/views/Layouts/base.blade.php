<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- page title -->
<title> {{infos()->name}} </title>
<!-- favicon -->
<link rel="icon" href="favicon.ico" />
<!-- bootstrap core CSS -->
<link rel="stylesheet" href={{asset("template/css/bootstrap.min.css")}}>
<!-- font awesome -->
<link href={{asset("template/css/all.min.css")}} rel="stylesheet">
<!-- owl carousel -->
<link href={{asset("template/css/owl.carousel.min.css")}} rel="stylesheet">
<link href={{asset("template/css/owl.theme.default.min.css")}} rel="stylesheet">
<!-- venobox css -->
<link rel="stylesheet" href={{asset("template/css/venobox.css")}}>
<!-- datepicker css -->
<link rel="stylesheet" href={{asset("template/css/datepicker.min.css")}}>
<!-- custom styles for this template -->
<link href={{asset("template/css/custom.css")}} rel="stylesheet">
<link href={{asset("template/css/responsive.css")}} rel="stylesheet">
<link href={{asset("template/css/helper.css")}} rel="stylesheet">
</head>

<body>
<!-- ================ Preloader ================ -->
<div id="preloader">
    <div class="spinner-grow" role="status"> <span class="sr-only">Loading...</span> </div>
</div>
<!-- ================ Preloader end ================ -->

<!-- ================ Header ================ -->
@include('Layouts.Header.header')
<!-- ================ Header end ================ -->

@yield('content')

<!-- ================ Footer area ================ -->
@include('Layouts.Footer.footer')
<!-- ================ Footer area end ================ -->

<!-- ================ Top scroll ================ -->
<a href="#" class="scroll-top">Top</a>
<!-- ================ Top scroll end ================ -->

<!-- js files -->
<script src={{asset("template/js/jquery-3.5.1.min.js")}}></script>
<script src={{asset("template/js/bootstrap.bundle.min.js")}}></script>
<!-- counter js -->
<script src={{asset("template/js/jquery-1.10.2.min.js")}}></script>
<script src={{asset("template/js/waypoints.min.js")}}></script>
<script src={{asset("template/js/jquery.counterup.min.js")}}></script>
<!-- venobox js -->
<script src={{asset("template/js/venobox.min.js")}}></script>
<!-- owl carousel -->
<script src={{asset("template/js/owl.carousel.min.js")}}></script>
<!-- portfolio js -->
<script src={{asset("template/js/jquery.mixitup.min.js")}}></script>
<!-- datepicker js -->
<script src={{asset("template/js/datepicker.min.js")}}></script>
<!-- script js -->
<script src={{asset("template/js/custom.js")}}></script>
</body>

<!-- Mirrored from rainbowdesign.in/themes/Stetho/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Mar 2021 15:18:11 GMT -->
</html>
