<footer class="pt-50">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 mb-30">
                    <!-- title -->
                    <div class="title mb-10">
                        <h3>A Propos</h3>
                    </div>
                    <!-- title end -->
                    <!-- text -->
                    <div class="text">
                        <p>L'hôtel MANSA conjugue à la fois une tradition de l’hébergement, une restauration classique
                            et exotique.
                        </p>
                    </div>
                    <!-- end text -->
                    <!-- footer social -->
                    <div class="footer-social">
                        @if (infos()->facebook)
                            <a href="{{infos()->facebook}}"><i class="fab fa-facebook-square"></i></a>
                        @endif
                        @if (infos()->instagram)
                            <a href="{{infos()->instagram}}"><i class="fab fa-instagram-square"></i></a>
                        @endif
                        @if (infos()->twitter)
                            <a href="{{infos()->twitter}}"><i class="fab fa-twitter-square"></i></a>
                        @endif
                        @if (infos()->youtube)
                            <a href="{{infos()->youtube}}"><i class="fab fa-youtube-square"></i></a>
                        @endif
                    </div>
                    <!-- footer social end -->
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 mb-30">
                    <!-- title -->
                    <div class="title mb-10">
                        <h3>Navigation</h3>
                    </div>
                    <!-- title end -->
                    <!-- footer link -->
                    <ul class="footer-link">
                        <li><i class="fas fa-caret-right"></i> <a href={{route('home')}}>Acceuil</a> </li>
                        <li><i class="fas fa-caret-right"></i> <a href={{route('about')}}>A Propos de nous</a> </li>
                        <li><i class="fas fa-caret-right"></i> <a href={{route('rooms')}}>Chambres</a> </li>
                        <li><i class="fas fa-caret-right"></i> <a href={{route('contact')}}>Contact</a> </li>
                    </ul>
                    <!-- footer link end -->
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 mb-30">
                    <!-- title -->
                    <div class="title mb-10">
                        <h3>Newsletter</h3>
                    </div>
                    <!-- title end -->
                    <!-- footer newsletter text -->
                    <div class="footer-newsletter-text">
                        <p>Souscrivez a notre messagerie pour etre informé a temp réel des activités de l'hotel.</p>
                    </div>
                    <!-- footer newsletter text end -->
                    <!-- footer newsletter form -->
                    <div class="footer-newsletter-form">
                        <form>
                            <input placeholder="Adresse Email..." type="text" required>
                            <button type="submit">Souscrire</button>
                        </form>
                    </div>
                    <!-- footer newsletter form end -->
                </div>
            </div>
        </div>
    </div>
    <!-- footer copyright -->
    <div class="copyright text-center mt-20">© <span class="current-year"></span> Hotel Mansa Tout droit réservé.</div>
    <!-- footer copyright end -->
</footer>
