<div class="footer-bottom wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <span class="copyright-text">Copyright © 2021 <a href="template/https://dexignzone.com/"
                        target="_blank">DexignZone</a>. All rights reserved.</span>
            </div>
        </div>
    </div>
</div>
