@extends('Layouts.base')

@section('content')
    <!-- ================ Inner banner ================ -->
    <div class="inner-banner inner-banner-bg pt-70 pb-40">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-8 col-md-8 mb-30">
                    <!-- page-title -->
                    <div class="page-title">
                        <h1>About Us</h1>
                    </div>
                    <!-- page-title end -->
                </div>
                <div class="col-lg-4 col-md-4 mb-30">
                    <!-- breadcrumb -->
                    <ol class="breadcrumb mb-0">
                        <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                        <li class="breadcrumb-item active">Apropos de Nous</li>
                    </ol>
                    <!-- breadcrumb end -->
                </div>
            </div>
        </div>
    </div>
    <!-- ================ Inner banner end ================ -->

    <!-- ================ About page ================ -->
    <div class="about-page pt-70 pb-60">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 mb-30">
                    <!-- about text -->
                    <div class="about-col">
                        <h6>Apropos de Nous</h6>
                        <h2><span> QUI SOMMES-NOUS </span></h2>
                        <p>Situé à la rue Pitol KAIGAMA, à 5 minutes du centre commercial, l’hôtel Mansa conjugue à la fois
                            une tradition de l’hébergement, une restauration classique et exotique. C’est l’un des plus
                            beaux patrimoines architecturaux et fonciers de la Région de l’Est-Cameroun.</p>
                        <p>Avec une capacité de 40 chambres, 5 suites et 1 appartement, l’Hôtel Mansa dispose. <br><br>
                            <ul>
                                <li><strong>Un bar panoramique ouvert sur la piscine ;</strong></li>
                                <li><strong>Un restaurant avec vue sur le lac Mansa ;</strong></li>
                                <li><strong>Une galerie marchande ;</strong></li>
                                <li><strong>Le wifi gratuit pour tous les clients ;</strong></li>
                                <li><strong>Un cour de tennis et de vastes espaces verts ;</strong></li>
                                <li><strong>Un parking sécurisé</strong></li>
                            </ul>
                        </p>
                        <a class=" btn-style-1" href="#" role="button">Explore More <i
                                class="fas fa-long-arrow-alt-right pl-6"></i></a>
                    </div>
                    <!-- about text end -->
                </div>
                <div class="col-lg-6 mb-30">
                    <!-- about video -->
                    <div class="espace-box position-relative rounded"> <img src="template/img/about-us/espace.jpg" alt=""
                            class="img-fluid rounded">
                        <div class="hoverlay d-flex flex-row align-items-center justify-content-center"> <a
                                class="venobox d-inline" data-gall="gall-video" data-autoplay="true" data-vbtype="video"
                                href="https://www.youtube.com/watch?v=668nUCeBHyY&amp;t=28s"><i
                                    class="fab fa-google-play"></i></a> </div>
                    </div>
                    <!-- about video end -->
                </div>
            </div>
        </div>
    </div>
    <!-- ================ About page end ================ -->

    <!-- ================ Our features ================ -->
    <div class="our-features pt-100 pb-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 mb-30">
                    <!-- feature img -->
                    <div class="feature-img"> <img src="template/img/about-us/piscine1.jpg" class="rounded" alt=""> </div>
                    <!-- feature img end -->
                </div>
                <div class="col-lg-6 mb-30">
                    <!-- feature text -->
                    <div class="feature-text">
                        <h2><span>Espaces de Detentes</span></h2>
                        <!-- features tabs -->
                        <div class="features-tabs">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" id="tab1-tab" data-toggle="tab"
                                        href="#tab1" role="tab" aria-controls="tab1" aria-selected="true"><i
                                            class="far fa-gem"></i>Piscine</a> </li>
                                <li class="nav-item"> <a class="nav-link" id="tab2-tab" data-toggle="tab" href="#tab2"
                                        role="tab" aria-controls="tab2" aria-selected="false"><i
                                            class="fas fa-desktop"></i>Stade de Tennis</a> </li>
                                <li class="nav-item"> <a class="nav-link" id="tab3-tab" data-toggle="tab" href="#tab3"
                                        role="tab" aria-controls="tab3" aria-selected="false"><i class="fas fa-users"></i>
                                        Musée</a> </li>
                            </ul>
                            <div class="tab-content pt-20" id="myTabContent">
                                <div class="tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="tab1-tab">
                                    <p>Un cadre trés approprié pour se detente et se divertir grace a une grande piscine
                                        adapteté a tout tranche d'age.</p>
                                    <p class="mb-0">Un maitre nageur present sur les lieux 7/7 pour assuré le bon
                                        divertissement de tout clients ou visiteurs.</p>
                                </div>
                                <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="tab2-tab">
                                    <p>Un grand stade de tennis tres adapté, pour maintenir l'equilibre physique de nos
                                        clients.</p>

                                </div>
                                <div class="tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="tab3-tab">
                                    <p>Lorem ipsum dolor sit amet consectetur adipiscing elit amet consectetur piscing elit
                                        amet consectetur adipiscing elit sed et eletum nulla eu placerat felis etiam ermum
                                        sit amet.</p>
                                    <p class="mb-0">Lorem ipsum dolor sit amet consectetur adipiscing elit amet consectetur
                                        piscing elit amet consectetur adipiscing elit sed et eletum nulla eu placerat felis
                                        etiam tincidunt orci lacus id varius dolor fermum sit amet.</p>
                                </div>
                            </div>
                        </div>
                        <!-- features tabs end -->
                    </div>
                    <!-- feature text end -->
                </div>
            </div>
        </div>
    </div>
    <!-- ================ Our features end ================ -->

    <!-- ================ Team page ================ -->
    <div class="team-page pt-70 pb-40">
        <div class="container">
            <h2><span> Notre Personnel </span></h2>
            <div class="row row-cols-lg-3 row-cols-md-2 row-cols-sm-2 row-cols-1">
                <div class="col mb-30">
                    <!-- team box -->
                    <div class="team-box">
                        <div class="team-img"><img src="template/img/team/atemk.jpg" alt=""></div>
                        <div class="team-des">
                            <h3><a href="#">ATEMKENG franck</a></h3>
                            <p class="mb-0">Fondateur</p>
                        </div>
                    </div>
                    <!-- team box end -->
                </div>
                <div class="col mb-30">
                    <!-- team box -->
                    <div class="team-box">
                        <div class="team-img"><img src="template/img/team/api.jpg" alt=""></div>
                        <div class="team-des">
                            <h3><a href="#">Visiteur</a></h3>
                            <p class="mb-0">Designer</p>
                        </div>
                    </div>
                    <!-- team box end -->
                </div>
                <div class="col mb-30">
                    <!-- team box -->
                    <div class="team-box">
                        <div class="team-img"><img src="template/img/team/foot3.jpg" alt=""></div>
                        <div class="team-des">
                            <h3><a href="#">Ekanga fernang</a></h3>
                            <p class="mb-0">Support-Marketing</p>
                        </div>
                    </div>
                    <!-- team box end -->
                </div>
                <div class="col mb-30">
                    <!-- team box -->
                    <div class="team-box">
                        <div class="team-img"><img src="template/img/team/simo.jpg" alt=""></div>
                        <div class="team-des">
                            <h3><a href="#">Simo negue</a></h3>
                            <p class="mb-0">CEO</p>
                        </div>
                    </div>
                    <!-- team box end -->
                </div>
                <div class="col mb-30">
                    <!-- team box -->
                    <div class="team-box">
                        <div class="team-img"><img src="template/img/team/chris.jpg" alt=""></div>
                        <div class="team-des">
                            <h3><a href="#">Djitsa christian</a></h3>
                            <p class="mb-0">Consultant</p>
                        </div>
                    </div>
                    <!-- team box end -->
                </div>
                <div class="col mb-30">
                    <!-- team box -->
                    <div class="team-box">
                        <div class="team-img"><img src="template/img/team/marina.jpg" alt=""></div>
                        <div class="team-des">
                            <h3><a href="#">Marina</a></h3>
                            <p class="mb-0">Cuisinier</p>
                        </div>
                    </div>
                    <!-- team box end -->
                </div>
            </div>
        </div>
    </div>
    <!-- ================ Team page end ================ -->

    <!-- ================ Testimonial page ================ -->
    <div class="testimonial-page pt-70 pb-40">
        <div class="container">
            <h2><span>Temoignages de nos clients</span></h2>
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <!-- testimonial box -->
                    <div class="testimonial-box position-relative shadow rounded mb-30">
                        <p class="text-muted border-bottom font-italic pb-3">"Mon sejour a mansa hotel a été très superbe
                            par la diversité des menus qu'il présente a leurs clients surtout leurs fameux repas
                            traditionnel le Kati-kati. "</p>
                        <div class="reviewer d-inline-block"> <img src="template/img/testimonial/simo.jpg"
                                class="float-left mr-3 rounded" alt="">
                            <div class="content d-block overflow-hidden">
                                <h4 class="name mb-0 text-uppercase">SIMO Valdez</h4>
                                <small class="designation text-muted">Developpeur Web</small>
                            </div>
                        </div>
                        <div class="quote-icon"><i class="fas fa-quote-left"></i></div>
                    </div>
                    <!-- testimonial box end -->
                </div>
                <div class="col-lg-6 col-md-6">
                    <!-- testimonial box -->
                    <div class="testimonial-box position-relative shadow rounded mb-30">
                        <p class="text-muted border-bottom font-italic pb-3">" Mansa Hotel a les lits les plus confortable
                            que j'ai vu de ma vie, très pratique pour une viré en couple. "</p>
                        <div class="reviewer d-inline-block"> <img src="template/img/testimonial/bouba.jpg"
                                class="float-left mr-3 rounded" alt="">
                            <div class="content d-block overflow-hidden">
                                <h4 class="name mb-0 text-uppercase">DJOUFACK Jordan</h4>
                                <small class="designation text-muted">Entrepreneur</small>
                            </div>
                        </div>
                        <div class="quote-icon"><i class="fas fa-quote-left"></i></div>
                    </div>
                    <!-- testimonial box end -->
                </div>
                <div class="col-lg-6 col-md-6">
                    <!-- testimonial box -->
                    <div class="testimonial-box position-relative shadow rounded mb-30">
                        <p class="text-muted border-bottom font-italic pb-3">" Grace a leur salle de sport tennis j'ai pu
                            bien m'épanouire par l'aide de leur super coatch. "</p>
                        <div class="reviewer d-inline-block"> <img src="template/img/testimonial/03.jpg"
                                class="float-left mr-3 rounded" alt="">
                            <div class="content d-block overflow-hidden">
                                <h4 class="name mb-0 text-uppercase">AKAMBA Stephane</h4>
                                <small class="designation text-muted">Entrepreneur</small>
                            </div>
                        </div>
                        <div class="quote-icon"><i class="fas fa-quote-left"></i></div>
                    </div>
                    <!-- testimonial box end -->
                </div>
                <div class="col-lg-6 col-md-6">
                    <!-- testimonial box -->
                    <div class="testimonial-box position-relative shadow rounded mb-30">
                        <p class="text-muted border-bottom font-italic pb-3">" Mansa Hotel est un endroit les plus chaud que
                            j'ai connu dans ma carriére, un endroit très accueillant et sportif. "</p>
                        <div class="reviewer d-inline-block"> <img src="template/img/testimonial/04.jpg"
                                class="float-left mr-3 rounded" alt="">
                            <div class="content d-block overflow-hidden">
                                <h4 class="name mb-0 text-uppercase">NGAMEN Cynthia</h4>
                                <small class="designation text-muted">Medecin</small>
                            </div>
                        </div>
                        <div class="quote-icon"><i class="fas fa-quote-left"></i></div>
                    </div>
                    <!-- testimonial box end -->
                </div>
                <div class="col-lg-6 col-md-6">
                    <!-- testimonial box -->
                    <div class="testimonial-box position-relative shadow rounded mb-30">
                        <p class="text-muted border-bottom font-italic pb-3">"A l’est du cameroun, je n’ai pas trouvé mieux;
                            le cadre convivial, les espaces détente soignés. Toutes les conditions sont réunies pour un
                            séjour agréable!. "</p>
                        <div class="reviewer d-inline-block"> <img src="template/img/testimonial/tala.jpg"
                                class="float-left mr-3 rounded" alt="">
                            <div class="content d-block overflow-hidden">
                                <h4 class="name mb-0 text-uppercase">AMADOU Tamto</h4>
                                <small class="designation text-muted">Directeur Radio Aurore</small>
                            </div>
                        </div>
                        <div class="quote-icon"><i class="fas fa-quote-left"></i></div>
                    </div>
                    <!-- testimonial box end -->
                </div>
                <div class="col-lg-6 col-md-6">
                    <!-- testimonial box -->
                    <div class="testimonial-box position-relative shadow rounded mb-30">
                        <p class="text-muted border-bottom font-italic pb-3">"Mansa hotel est un coin des plus exquis. J’ai
                            particulièrement apprécié l’option du room service."</p>
                        <div class="reviewer d-inline-block"> <img src="template/img/testimonial/06.jpg"
                                class="float-left mr-3 rounded" alt="">
                            <div class="content d-block overflow-hidden">
                                <h4 class="name mb-0 text-uppercase">Boeman Cambel</h4>
                                <small class="designation text-muted">Touriste</small>
                            </div>
                        </div>
                        <div class="quote-icon"><i class="fas fa-quote-left"></i></div>
                    </div>
                    <!-- testimonial box end -->
                </div>
            </div>
        </div>
    </div>
    <!-- ================ Testimonial page end ================ -->
@endsection
